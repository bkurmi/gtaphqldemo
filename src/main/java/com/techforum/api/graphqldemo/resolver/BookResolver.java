package com.techforum.api.graphqldemo.resolver;

import org.springframework.stereotype.Component;

import com.techforum.api.graphqldemo.model.Book;

import graphql.kickstart.tools.GraphQLQueryResolver;

@Component
public class BookResolver implements GraphQLQueryResolver{

	public Book getBook(String isbn) {
		return new Book("LearnGraphQL", "234435654");
	}
}
